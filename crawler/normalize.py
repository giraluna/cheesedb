from unidecode import unidecode

import cheesedb


with cheesedb.Connector() as dbh:
  dbh.cursor.execute("""
      SELECT * FROM cheeses
    """)

  allCheeses = dbh.cursor.fetchall()

  for cheese in allCheeses:
    dbh.cursor.execute("""
      UPDATE cheeses
      SET ascii_name = %(decoded)s
      WHERE name = %(undecoded)s
    """,
    {
      "decoded": unidecode(cheese[0]),
      "undecoded": cheese[0]
    })

  dbh.connection.commit()
