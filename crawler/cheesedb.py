import psycopg2
import json

class Connector:
  def __init__(self):
    credentials = None
    with open("dbcredentials.json") as jsonData:
      credentials = json.load(jsonData)
    
    self.connection = psycopg2.connect(
      host = "localhost",
      database = "cheese",
      user = credentials["userName"],
      password = credentials["password"]
    )
    self.cursor = self.connection.cursor()

  def __enter__(self):
    return self
    
  def insertCheese(self, name,
    country=None, animals=None, biota=None, hardness=None):
    self.cursor.execute("""
      INSERT INTO cheeses
      (
        name,
        origin_country,
        animal,
        biota,
        hardness
      )
      SELECT
        %(name)s,
        %(origin_country)s,
        %(animal)s,
        %(biota)s,
        %(hardness)s
      WHERE
        NOT EXISTS
        (
          SELECT name FROM cheeses WHERE name = %(name)s
        )
      """,
      {
        "name": name,
        "origin_country": country,
        "animal": list(animals) if bool(animals) else None,
        "biota": list(biota) if bool(biota) else None,
        "hardness": hardness
      }
    )

    self.connection.commit()

  def close(self):
    self.cursor.close()
    self.connection.close()

  def __exit__(self, type, value, traceback):
    self.close()

  def __del__(self):
    self.close()
