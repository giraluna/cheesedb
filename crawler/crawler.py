from bs4 import BeautifulSoup

import urllib.request
import urllib.parse
import urllib.robotparser

import time
import re
import sys
import os

import parsecountries
import cheesedb


animalAliases = {
  "cow": "cow",
  "sheep": "sheep",
  "ewe": "sheep",
  "goat": "goat",
  "buffalo": "water buffalo",
  "yak": "yak",
  "camel": "camel",
  "donkey": "donkey",
  "moose": "moose",
  "reindeer": "reindeer"
}

hardnesses = [
  "soft",
  "firm",
  "hard",

  "semi-"
]

countryAliases = parsecountries.getCountryAliases()


class Crawler:
  def __init__(self, seedUrl, crawlDelay):

    self.crawlDelay = crawlDelay
    self.userAgentName = "CheeseCrawler"
    self.crawlsLeft = 0

    self.crawledFile = open("crawled.txt", "a+")
    self.crawledSet = self.buildSetFromFile(self.crawledFile)

    self.toCrawlFile = open("tocrawl.txt", "a+")
    self.toCrawlSet = self.buildSetFromFile(self.toCrawlFile).difference(self.crawledSet)

    self.databaseConnector = cheesedb.Connector()

    url = self.normalizeUrl(seedUrl)
    
    self.initRobotParser(url)
    
    if not self.hasUrl(url):
      self.addUrlToCrawl(url)

    self.nextCrawlTime = time.time()

    if len(self.toCrawlSet) == 0:
      print("!!!No pages to crawl")

  def buildSetFromFile(self, file):
    resultSet = set()
    file.seek(0, 0)
    for line in file:
      resultSet.add(line.strip())

    file.seek(0, 2)
    return resultSet


  def initRobotParser(self, url):
    netloc = urllib.parse.urlparse(url).netloc
    robotsUrl = netloc + "/robots.txt"

    self.robotParser = urllib.robotparser.RobotFileParser()

  def normalizeUrl(self, url):
    parsed = urllib.parse.urlparse(url)
    netloc = parsed.netloc or "en.wikipedia.org"
    return "http://" + netloc + parsed.path


  def hasUrl(self, url):
    if url in self.toCrawlSet or url in self.crawledSet:
      return True
    else:
      return False

  def addBiota(self, name):
    if name in self.biotaSet:
      return
    else:
      self.biotaSet.add(name)
      self.biotaFile.write(name + "\n")

  def addUrlToCrawl(self, url):
    if self.hasUrl(url):
      return
    else:
      linkType = self.classifyUrl(url)
      if not linkType:
        return
      elif not self.robotParser.can_fetch(self.userAgentName, url):
        return
      else:
        self.toCrawlSet.add(url)

        self.toCrawlFile.write(url + "\n")
        self.toCrawlFile.flush()
        os.fsync(self.toCrawlFile)
        print("Write toCrawl: " + url)

  def classifyUrl(self, url):
    parsed = urllib.parse.urlparse(url)
    if not "en.wikipedia.org" in parsed.netloc:
      return None
    elif not "/wiki/" in parsed.path:
      return None
    elif parsed.path.startswith("Category:"):
      if "cheeses" in parsed.path.lower():
        return "category"
      else:
        return None
    elif ":" in parsed.path:
      return None
    elif "list" in parsed.path.lower():
      if "cheeses" in parsed.path.lower():
        return "list"
      else:
        return None
    elif any(x in parsed.path.lower() for x in ["company", "cuisine", "province", "region"]):
      return None
    else:
      return "article"

  def addCrawledUrl(self, url):
    if url in self.crawledSet:
      return

    self.crawledSet.add(url)
    
    self.crawledFile.write(url + "\n")
    self.crawledFile.flush()
    os.fsync(self.crawledFile)
    print("Write crawled: " + url)

    if url in self.toCrawlSet:
      self.toCrawlSet.remove(url)


  def classifyPage(self, url, soup):
    parsed = urllib.parse.urlparse(url)
    if "Category:" in parsed.path and "cheese" in parsed.path.lower():
      return "category"

    if "list" in parsed.path.lower() and "cheeses" in parsed.path.lower():
      return "list"

    if soup.find(class_="biota"):
      return "organism"


    catlinks = soup.find(id="mw-normal-catlinks")
    if not catlinks:
      return None

    categories = catlinks.find_all("a")

    for category in categories:
      categoryHref = category.get("href")
      if re.search("cheeses", categoryHref, re.IGNORECASE):
        return "cheese"
      if re.search("brined cheese", categoryHref, re.IGNORECASE):
        return "cheese"

    return None

  def getWordsByMentionCountIn(self, soup, wordIndex, weight=1):
    wordsByCount = {}

    for word, alias in wordIndex.items():
      if not alias in wordsByCount:
        wordsByCount[alias] = 0

      matches = soup.find_all(text=re.compile("\\b" + word + "s*\\b", re.IGNORECASE))

      wordsByCount[alias] += len(matches) * weight

    return wordsByCount

  def getMilkTypesFromSoup(self, soup):
    animalsByCount = self.findWordOccurencesInSoup(soup, animalAliases)
    
    milkSources = set()
    animalsWithMatches = set()
    self.addKeysThatHaveValueToSet(animalsWithMatches, animalsByCount)

    mentionsNeeded = 3 if len(animalsWithMatches) > 0 else 1

    for animal, count in animalsByCount.items():
      if count >= mentionsNeeded and animal not in milkSources:
        milkSources.add(animal)

    return milkSources

  def getHardnessFromSoup(self, soup):
    hardnessesByCount = self.findWordOccurencesInSoup(soup, hardnesses)

    isSemi = hardnessesByCount["semi-"] >= 2

    hardnessesByCount["semi-"] = 0

    bestMatch = self.getItemWithMaxValueFromDict(hardnessesByCount)

    hardnessString = bestMatch[0]
    if isSemi:
      hardnessString = "semi-" + hardnessString

    if bestMatch[1] < 2:
      return None
    else:
      return hardnessString

  def getCountryFromSoup(self, soup):
    countriesByCount = self.findWordOccurencesInSoup(soup, countryAliases, True)

    bestMatch = self.getItemWithMaxValueFromDict(countriesByCount)

    if bestMatch[1] == 0:
      return None
    else:
      return bestMatch[0]

  def getNameFromSoup(self, soup):
    return soup.find(id="firstHeading").get_text()

  def findWordOccurencesInSoup(self, soup, wordlist, earlyReturn = False):
    wordIndex = None
    matchesByWord = None

    if isinstance(wordlist, dict):
      matchesByWord = dict.fromkeys(wordlist.values(), 0)
      wordIndex = wordlist
    elif isinstance(wordlist, list):
      matchesByWord = dict.fromkeys(wordlist, 0)
      wordIndex = {key: key for key in wordlist}


    infoboxes = soup.find_all(class_="infobox")
    categories = soup.find(id="mw-normal-catlinks").find_all("a")

    definiteSources = infoboxes + categories

    for source in definiteSources:
      sourceWords = self.getWordsByMentionCountIn(source, wordIndex, 10)
      self.sumDictionaries(matchesByWord, sourceWords)


    if earlyReturn:
      matchesSoFar = self.addKeysThatHaveValueToSet(set(), matchesByWord)
      if len(matchesSoFar) > 0:
        return matchesByWord

    content = soup.find(id="mw-content-text")
    paragraphs = content.find_all("p", recursive=False)
    lists = content.find_all("ul", recursive=False) + content.find_all("ol", recursive=False)

    possibleSources = paragraphs + lists

    for source in possibleSources:
      self.sumDictionaries(matchesByWord, self.getWordsByMentionCountIn(source, wordIndex))

    return matchesByWord

  def sumDictionaries(self, a, b):
    for key in b.keys():
      if not key in a:
        a[key] = b[key]
      else:
        a[key] += b[key]

  def addKeysThatHaveValueToSet(self, s, d):
    for key, value in d.items():
      if value:
        s.add(key)
    return s

  def getItemWithMaxValueFromDict(self, d):
    v = list(d.values())
    k = list(d.keys())
    maxV = max(v)
    return (k[v.index(maxV)], maxV)

  def getLinkUrls(self, soup, allowedTypes):
    allUrls = set()

    for link in soup.find_all("a"):
      url = link.get("href")

      if not url:
        continue

      normalized = self.normalizeUrl(url)

      if not normalized in allUrls:
        linkType = self.classifyUrl(normalized)
        if linkType in allowedTypes:
          allUrls.add(normalized)


    return allUrls

  def addPageToDatabase(self, url, soup):

    name = self.getNameFromSoup(soup)
    milks = self.getMilkTypesFromSoup(soup)
    if len(milks) < 1:
      milks = None
      milkString = "None"
    else:
      milkString = ", ".join(milks)
      
    hardness = self.getHardnessFromSoup(soup)
    country = self.getCountryFromSoup(soup)

    infoString = name.center(26) + "| " + milkString.center(24) + "| " + str(hardness).center(10) + "| " + str(country).center(14)
    print(infoString)

    self.databaseConnector.insertCheese(name, country, milks, None, hardness)

  def processOnce(self):
    if len(self.toCrawlSet) > 0:
      urlToCrawl = self.toCrawlSet.pop()
    else:
      print("!!!No urls left to process")
      return
    
    timeUntilNextCrawl = self.nextCrawlTime - time.time()
    if (timeUntilNextCrawl > 0):
      time.sleep(timeUntilNextCrawl)

    if self.hasUrl(urlToCrawl):
      print("!!! Conflicting url: " + urlToCrawl)
    elif not self.classifyUrl(urlToCrawl):
      print("!!! Uninteresting url: " + urlToCrawl)
    else:
      self.crawlUrl(urlToCrawl)
      self.nextCrawlTime = time.time() + self.crawlDelay

    if self.crawlsLeft < 0 or self.crawlsLeft > 0:
      self.crawlsLeft -= 1
      self.processOnce()

  def crawlUrl(self, url):

    print(str(time.time()) + "Crawled url: " + url)

    response = urllib.request.urlopen(url)
    soup = BeautifulSoup(response)

    allowedLinkTypes = set(["list", "category"])

    pageClassification = self.classifyPage(url, soup)
    if pageClassification == "category" or pageClassification == "list":
      allowedLinkTypes.add("article")
    elif pageClassification == "cheese":
      self.addPageToDatabase(url, soup)

    linksToFollow = self.getLinkUrls(soup, allowedLinkTypes)

    for link in linksToFollow:
      self.addUrlToCrawl(link)

    self.addCrawledUrl(url)

  def closeFiles(self):
    self.crawledFile.close()
    self.toCrawlFile.close()

  def __del__(self):
    self.closeFiles()
    print("del")
    with open("tocrawl.delbackup", "w") as delBackup:
      for url in self.toCrawlSet:
        delBackup.write(url + "\n")

spider = Crawler("http://en.wikipedia.org/wiki/List_of_cheeses", 10)
