def getCountryAliases():

  countryAliases = {}

  with open("countries.txt", encoding="utf-8") as countries:
    for line in countries:
      splitIndex = line.find("ÄÄÄ")

      countryName = line[0:splitIndex].strip()
      if not countryName:
        continue
        
      countryAliases[countryName] = countryName

      adjectivesString = line[splitIndex + 3:]
      countryAdjectives = adjectivesString.split(",")
      for adjective in countryAdjectives:
        strippedAdjective = adjective.strip()
        if strippedAdjective:
          countryAliases[strippedAdjective] = countryName

  return countryAliases
