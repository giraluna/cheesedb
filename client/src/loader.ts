/// <reference path="requester.ts" />

module Cheese
{
  export class Loader
  {
    loaded:
    {
      DOM: boolean;
    } =
    {
      DOM: false
    };
    jsonData: any;
    onLoaded: (Loader) => void;

    constructor(onLoaded: (Loader) => void)
    {
      this.onLoaded = onLoaded;
      this.loadDOM();
    }
    loadIfAllDone()
    {
      for (var prop in this.loaded)
      {
        if (!this.loaded[prop])
        {
          return false;
        }
      }

      this.jsonData = JSON.parse(document.getElementById("initial-search-data").innerHTML);

      this.onLoaded(this);
    }
    loadDOM()
    {
      var self = this;
      if (document.readyState === "interactive" || document.readyState === "complete")
      {
        self.loaded.DOM = true;
        self.loadIfAllDone();
      }
      else
      {
        document.addEventListener('DOMContentLoaded', function()
        {
          self.loaded.DOM = true;
          self.loadIfAllDone();
        });
      }
    }
  }
}
