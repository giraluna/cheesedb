module Cheese
{
  export interface ICheeseData
  {

  }
  export class Requester
  {
    onSuccess: (any) => void;
    onFailure: (any) => void;
    timesToRetry: number;
    timesRetried: number = 0;
    queryString: string;

    constructor(
      // onSuccess: (any) => void,
      // onFailure: (any) => void,
      timesToRetry: number)
    {
      // this.onSuccess = onSuccess;
      // this.onFailure = onFailure;
      this.timesToRetry = timesToRetry;
    }

    handleFailure(response: any)
    {
      if (this.timesRetried < this.timesToRetry)
      {
        this.makeSearchRequest(this.queryString, true);
      }
      else
      {
        this.onFailure(response);
      }
    }

    makeSearchRequest(queryString: string, isRetry: boolean = false)
    {
      var self = this;
      this.queryString = queryString;

      if (isRetry)
      {
        this.timesRetried++;
      }
      else
      {
        this.timesRetried = 0;
      }

      var request = new XMLHttpRequest();
      request.open("get", "/search?" + this.queryString, true);

      request.onload = function()
      {
        if (request.status >= 200 && request.status < 400)
        {
          self.onSuccess(JSON.parse(request.response));
        }
        else
        {
          self.handleFailure(request);
        }
      }

      request.onerror = this.handleFailure;
      request.send();
    }
  }
  
}
