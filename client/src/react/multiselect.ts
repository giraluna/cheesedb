/// <reference path="multiselectoption.ts" />

module Cheese
{
  export module UIComponents
  {
    export var MultiSelect = React.createClass(
    {
      displayName: "MultiSelect",

      getInitialState: function()
      {
        return(
        {
          selected: [],
          selectionMode: this.props.initialSelectionMode || "ANY",
          availableModes: this.props.availableModes || ["ANY", "ALL", "NOT"],
          displayString: ""
        });
      },

      getValuesFromString: function(source: string)
      {
        return (this.props.options.filter(function(value)
        {
          return source.indexOf(value) !== -1;
        }));
      },

      changeSelectionMode: function(e)
      {
        this.setState({selectionMode: e.target.value}, this.props.onChange);
      },

      setDisplayString: function(e)
      {
        this.setState(
        {
          displayString: e.target.value,
          selected: this.getValuesFromString(e.target.value)
        }, this.props.onChange);
      },

      makeDisplayString: function(selected: string[] = this.state.selected)
      {
        return selected.join(", ");
      },

      getQueryString: function()
      {
        if (this.state.selected.length === 0)
        {
          return null;
        }
        else if (this.state.selected.length === 1 && this.state.selectionMode !== "NOT")
        {
          return this.state.selected[0];
        }
        else
        {
          return this.state.selectionMode + "(" + this.makeDisplayString() + ")";
        }
      },

      toggleValue: function(value)
      {
        var index = this.state.selected.indexOf(value);
        var newSelected = this.state.selected.slice(0);

        if (index !== -1)
        {
          newSelected.splice(index, 1);
        }
        else
        {
          newSelected.push(value);
        }

        newSelected.sort();

        this.setState(
        {
          selected: newSelected,
          displayString: this.makeDisplayString(newSelected)
        }, this.props.onChange);

        return false;
      },

      clearSelection: function()
      {
        this.setState(
        {
          selected: [],
          displayString: ""
        }, this.props.onChange);
      },
      
      render: function()
      {
        var controller = null;
        if (this.props.hasController)
        {
          var selectionModeOptions = [];

          for (var i = 0; i < this.state.availableModes.length; i++)
          {
            var mode = this.state.availableModes[i];
            selectionModeOptions.push(React.DOM.option(
            {
              value: mode,
              key: mode
            },
              mode
            ))
          }
          controller = React.DOM.div(
          {
            className: "multi-select-controller"
          },
            React.DOM.select(
            {
              className: "multi-select-controller-mode",
              value: this.state.selectionMode,
              onChange: this.changeSelectionMode
            },
              selectionModeOptions
            ),
            React.DOM.input(
            {
              className: "multi-select-controller-input",
              value: this.state.displayString,
              onChange: this.setDisplayString
            })
            /*
            React.DOM.button(
            {
              className: "multi-select-controller-clear",
              onClick: this.clearSelection
            }, "X")
            */
          )
        }

        var options = [];
        for (var i = 0; i < this.props.options.length; i++)
        {
          var option = this.props.options[i];
          options.push(UIComponents.MutliSelectOption(
          {
            onSelect: this.toggleValue.bind(this, option),
            key: option,
            label: option,
            isChecked: this.state.selected.indexOf(option) !== -1
          }));
        }

        return(
          React.DOM.div(
          {
            className: "multi-select"
          },
            controller,
            React.DOM.ol(
            {
              className: "multi-select-options"
            },
              options
            )
          )
        );
      }
    })
  }
}
