/// <reference path="../../lib/react.d.ts" />
/// <reference path="search.ts" />

module Cheese
{
  export module UIComponents
  {
    export var Site = React.createClass(
    {
      displayName: "Site",

      render: function()
      {
        var componentToRender;
        switch (this.props.currentScene)
        {
          case ReactScene.search:
          {
            componentToRender = UIComponents.Search(
            {
              searchData: this.props.searchData,
              requester: this.props.requester
            });
            break;
          }
        }

        return(
          React.DOM.div(
          {
            className: "site"
          },
            componentToRender
          )
        );
      }
    })
  }
}
