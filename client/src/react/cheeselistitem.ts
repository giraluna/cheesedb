module Cheese
{
  export module UIComponents
  {
    export var CheeseListitem = React.createClass(
    {
      displayName: "CheeseListitem",

      shouldComponentUpdate: function(newProps: any)
      {
        return this.props.url !== newProps.url;
      },

      makeCell: function(type: string)
      {
        var className = "cheese-list-item-cell" + " cheese-list-item-" + type;
        var content;
        switch (type)
        {
          case "name":
          {
            content = React.DOM.a(
            {
              target: "_blank",
              href: this.props.url
            },
              this.props[type]
            )
            break;
          }
          case "animal":
          {
            content = this.props[type].join(", ");
            break;
          }
          default:
          {
            content = this.props[type];
            break;
          }
        }

        return(
          React.DOM.td(
          {
            key: type,
            className: className,
            title: this.props[type],
            "data-column": type
          }, content)
        );
      },

      render: function()
      {
        var columns = this.props.activeColumns;

        var cells = [];

        for (var i = 0; i < columns.length; i++)
        {
          var cell = this.makeCell(columns[i].key);
          cells.push(cell);
        }

        return(
          React.DOM.tr({className: "cheese-list-item"},
            cells
          )
        );
      }
    })
  }
}
