/// <reference path="list.ts" />
/// <reference path="cheeselistitem.ts" />

module Cheese
{
  export module UIComponents
  {
    enum enumHardness
    {
      "soft",
      "semi-soft",
      "firm",
      "semi-hard",
      "hard",
      "null"
    }
    export var CheeseList = React.createClass(
    {
      displayName: "CheeseList",

      getInitialState: function()
      {
        return(
        {
          hasMobileLinks: false
        });
      },
      

      componentDidMount: function()
      {
        window.addEventListener("resize", this.handleResize, true);
      },

      componentWillUnmount: function()
      {
        window.removeEventListener("resize", this.handleResize)
      },

      handleResize: function(e)
      {
        if (window.innerWidth <= 680)
        {
          this.setState({hasMobileLinks: true});
        }
        else if (window.innerWidth > 680)
        {
          this.setState({hasMobileLinks: false});
        }

        this.lastWidth = window.innerWidth;
      },

      render: function()
      {
        var urlPrefix = (this.state.hasMobileLinks ? "m." : "");
        var rows = [];
        for (var i = 0; i < this.props.cheeseData.length; i++)
        {
          var cheese = this.props.cheeseData[i];
          var data =
          {
            name: cheese.name,
            url: "http://en." + urlPrefix + "wikipedia.org/wiki/" + cheese.name,
            country: cheese.country,
            animal: cheese.animals,
            animalsSort: (cheese.animals.length === 0 ? ["zzz"] : cheese.animals),
            hardness: cheese.hardness,
            enumHardness: enumHardness[cheese.hardness],

            rowConstructor: UIComponents.CheeseListitem
          }

          rows.push(
          {
            key: cheese.name,
            data: data
          });
        }

        var columns =
        [
          {
            label: "Name",
            key: "name",
            defaultOrder: "asc"
          },
          {
            label: "Country",
            key: "country",
            defaultOrder: "asc"
          },
          {
            label: "Milk",
            key: "animal",
            defaultOrder: "asc",
            propToSortBy: "animalsSort"
          },
          {
            label: "Texture",
            key: "hardness",
            defaultOrder: "asc",
            propToSortBy: "enumHardness"
          }
        ]

        return(
          React.DOM.div(
          {
            className: "cheese-list"
          },
            UIComponents.List(
            {
              listItems: rows,
              initialColumns: columns,
              onSelectColumn: this.props.onSelectColumn,
              preventAutoResize: true
            })
          )
        );
      }
    })
  }
}
