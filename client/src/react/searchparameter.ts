module Cheese
{
  export module UIComponents
  {
    export var SearchParameter = React.createClass(
    {
      displayName: "SearchParameter",
      
      shouldComponentUpdate: function(newProps: any)
      {
        return newProps.value !== this.props.value;
      },

      handleClear: function()
      {

      },

      render: function()
      {
        return(
          React.DOM.div(
          {
            className: "search-parameter-container"
          },
            React.DOM.div(
            {
              className: "search-parameter-label"
            },
              this.props.label
            ),
            this.props.children
          )
        );
      }
    })
  }
}
