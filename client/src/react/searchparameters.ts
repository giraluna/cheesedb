/// <reference path="searchparameter.ts" />
/// <reference path="multiselect.ts" />

module Cheese
{
  export module UIComponents
  {
    export var SearchParameters = React.createClass(
    {
      displayName: "SearchParameters",
      getInitialState: function()
      {
        return(
        {
          animal: [],
          country: null,
          name: null,
          hardness: [],
          animalString: "",
          hardnessString: ""
        });
      },

      getSelectedValues: function(selectElement)
      {
        var values = [];
        for (var i = 0; i < selectElement.selectedOptions.length; i++)
        {
          var value = selectElement.selectedOptions[i].value;
          if (value) values.push(value);
        }

        return values;
      },

      makeMultiSelectQueryString: function(values: string[], prefix: string)
      {
        var queryString = "";

        if (values && values.length > 0)
        {

          if (values.length === 1)
          {
            queryString += values[0];
          }
          else
          {
            queryString += prefix + values.join(",") + ")";
          }
        }

        return queryString;
      },

      handleNameChange: function(e)
      {
        this.setState({name: e.target.value}, this.props.delayedOnUpdate);
      },

      handleCountryChange: function(e)
      {
        this.setState({country: e.target.value}, this.props.delayedOnUpdate);
      },

      handleAnimalChange: function()
      {
        var animalSelect = this.refs["animalSelect"];

        this.setState(
        {
          animal: animalSelect.state.selected,
          animalString: animalSelect.getQueryString()
        }, this.props.onUpdate);
      },

      handleHardnessChange: function()
      {
        var hardnessSelect = this.refs["hardnessSelect"];

        this.setState(
        {
          hardness: hardnessSelect.state.selected,
          hardnessString: hardnessSelect.getQueryString()
        }, this.props.onUpdate);
      },
      
      render: function()
      {
        return(
          React.DOM.div(
          {
            className: "search-parameters"
          },
            // NAME
            UIComponents.SearchParameter(
            {
              label: "Name"
            },
              React.DOM.input(
              {
                className: "search-parameter-input",
                value: this.state.name,
                placeholder: "Cheese name",
                onChange: this.handleNameChange
              })
            ),

            // COUNTRY
            UIComponents.SearchParameter(
            {
              label: "Origin country"
            },
              React.DOM.input(
              {
                className: "search-parameter-input",
                value: this.state.country,
                placeholder: "Origin country",
                onChange: this.handleCountryChange
              })
            ),

            // ANIMAL
            UIComponents.SearchParameter(
            {
              label: "Source(s) of milk"
            },
              UIComponents.MultiSelect(
              {
                onChange: this.handleAnimalChange,
                ref: "animalSelect",
                hasController: true,
                initialSelectionMode: "ALL",
                options: ["cow", "sheep", "goat", "water buffalo",
                  "yak", "camel", "donkey", "moose"]
              })
            ),

            // HARDNESS
            UIComponents.SearchParameter(
            {
              label: "Texture"
            },
              UIComponents.MultiSelect(
              {
                onChange: this.handleHardnessChange,
                ref: "hardnessSelect",
                initialSelectionMode: "ANY",
                availableModes: ["ANY", "NOT"],
                options: ["soft", "semi-soft", "firm", "semi-hard", "hard"]
              })
            )
          )
        );
      }
    })
  }
}
