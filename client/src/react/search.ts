/// <reference path="../utility.ts" />

/// <reference path="searchparameters.ts" />
/// <reference path="cheeselist.ts" />
/// <reference path="pagenavigation.ts" />

module Cheese
{
  export module UIComponents
  {
    export var Search = React.createClass(
    {
      displayName: "Search",
      mixins: [React.addons.PureRenderMixin],

      componentDidMount: function()
      {
        this.props.requester.onSuccess = this.handleSearchSuccess;
        this.props.requester.onFailure = this.handleSearchFailure;

        this.delayedCallbacks = {};
      },

      getInitialState: function()
      {
        return(
        {
          currentPage: 1,
          offset: 0,
          totalPages: Math.ceil(this.props.searchData.matchCount / 20),
          searchData: this.props.searchData,
          itemsPerPage: 20,
          isFetching: false
          // lastQueryString: "",
          // sortBy: "name",
          // order: "asc"
        });
      },

      componentWillUnmount: function()
      {
        for (var tag in this.delayedCallbacks)
        {
          window.clearTimeout(this.delayedCallbacks[tag]);
          this.delayedCallbacks[tag] = null;
        }

        this.props.requester.onSuccess = null;
        this.props.requester.onFailure = null;
      },

      makeQueryString: function()
      {
        var queryString = "";
        queryString += "limit=" + this.state.itemsPerPage;
        queryString += "&offset=" + this.state.offset;

        var params = this.refs.params.state;


        ["animal", "hardness"].forEach(function(param)
        {
          if (params[param] && params[param].length > 0)
          {
            var paramString = params[param + "String"];
            var q = param === "hardness" ? "\"" : "";

            queryString += "&" + param + "=" + q + paramString + q;
          }
        });

        
        ["name", "country"].forEach(function(param)
        {
          if (params[param])
          {
            queryString += "&" + param + "=" + params[param];
          }
        });

        if (this.state.sortBy)
        {
          queryString += "&sortBy=" + this.state.sortBy;
        }
        if (this.state.order)
        {
          queryString += "&order=" + this.state.order;
        }


        return queryString;
      },

      handleParameterUpdate: function(resetPosition: boolean = false)
      {
        if (resetPosition)
        {
          this.shouldResetPosition = true;
        }

        if (this.shouldResetPosition)
        {
          this.shouldResetPosition = false;
          this.setState(
          {
            offset: 0,
            currentPage: 1
          }, this.executeSearch);
        }
        else
        {
          this.executeSearch();
        }
      },

      executeDelayed: function(tag: string, delay: number, callback)
      {
        if (this.delayedCallbacks[tag])
        {
          window.clearTimeout(this.delayedCallbacks[tag]);
        }

        this.delayedCallbacks[tag] = window.setTimeout(function()
        {
          callback();
          this.delayedCallbacks[tag] = null
        }.bind(this), delay);
      },

      handleDelayedUpdate: function(delay: number = 500)
      {
        this.executeDelayed("search", delay, this.handleParameterUpdate.bind(this, true));
      },

      executeSearch: function()
      {
        var newQueryString = this.makeQueryString();

        if (this.lastQueryString === newQueryString) return;

        this.props.requester.makeSearchRequest(newQueryString);

        this.lastQueryString = newQueryString;
        this.setState(
        {
          isFetching: true
        });
      },

      handleSearchSuccess: function(searchData)
      {
        this.setState(
        {
          searchData: searchData,
          totalPages: Math.ceil(searchData.matchCount / this.state.itemsPerPage),
          isFetching: false
        }, this.afterSuccessfulSearch);
      },

      afterSuccessfulSearch: function()
      {
        if (this.afterSearchCallback)
        {
          this.afterSearchCallback();
          this.afterSearchCallback = null;
        }
      },

      handleSearchFailure: function(errorData)
      {
        console.error("Search failed");
        console.error(errorData);

        this.setState({isFetching: false});
      },

      handleSetPage: function(newPage: number, shouldDelay: boolean = false)
      {
        if (this.state.currentPage === newPage) return;

        newPage = clamp(newPage, 1, this.state.totalPages);


        var callback = shouldDelay ? this.handleDelayedUpdate : this.handleParameterUpdate;


        this.setState(
        {
          currentPage: newPage,
          offset: (newPage - 1) * this.state.itemsPerPage
        }, callback);
      },

      handleShiftPage: function(amount: number, shouldDelay: boolean = false)
      {
        this.handleSetPage(this.state.currentPage + amount, shouldDelay);
      },

      handleSetLimitPerPage: function(e)
      {
        var amount = clamp(e.target.value, 10, 100);


        this.setState(
        {
          itemsPerPage: amount,
          totalPages: Math.ceil(this.state.searchData.matchCount / amount),
          currentPage: (this.state.offset === 0 ? 1 : 1 + Math.ceil(this.state.offset / amount))
        }, this.handleParameterUpdate);
      },

      handleSelectColumn: function(column, callback)
      {
        if (this.state.totalPages < 2)
        {
          callback();
        }
        else
        {
          this.afterSearchCallback = callback;

          this.setState({sortBy: column.key, order: column.order},
            this.handleParameterUpdate.bind(this, true));
        }
      },

      render: function()
      {
        return(
          React.DOM.div(
          {
            className: "search-container"
          },
            UIComponents.SearchParameters(
            {
              ref: "params",
              onUpdate: this.handleParameterUpdate.bind(this, true),
              delayedOnUpdate: this.handleDelayedUpdate
            }),
            React.DOM.div(
            {
              className: "search-contents"
            },
              React.DOM.div(
              {
                className: "search-results-wrapper" + (this.state.isFetching ? " fetching" : "")
              },
                UIComponents.CheeseList(
                {
                  cheeseData: this.state.searchData.cheeses,
                  onSelectColumn: this.handleSelectColumn
                }),
                this.state.isFetching ? React.DOM.div(
                {
                  className: "search-results-fetching-wrapper"
                },
                  React.DOM.img(
                  {
                    className: "search-results-fetching-image",
                    src: "img\/ajax-loader.gif"
                  })
                ) : null
              ),
              React.DOM.div(
              {
                className: "search-navigation-wrapper"
              },
                React.DOM.div(
                {
                  className: "search-navigation-inner"
                },
                  UIComponents.PageNavigation(
                  {
                    currentPage: this.state.currentPage,
                    totalPages: this.state.totalPages,
                    onSetPage: this.handleSetPage,
                    onShiftPage: this.handleShiftPage
                  }),
                  React.DOM.div(
                  {
                    className: "search-navigation-per-page-container"
                  },
                    React.DOM.label(
                    {
                      className: "search-navigation-per-page-label",
                      htmlFor: "per-page-select"
                    },
                      "Results per page:"
                    ),
                    React.DOM.select(
                    {
                      id: "per-page-select",
                      value: this.state.itemsPerPage,
                      onChange: this.handleSetLimitPerPage
                    },
                      React.DOM.option({value: 10}, 10),
                      React.DOM.option({value: 20}, 20),
                      React.DOM.option({value: 40}, 40),
                      React.DOM.option({value: 100}, 100)
                    )
                  )
                )
              )
            )
          )
        );
      }
    })
  }
}
