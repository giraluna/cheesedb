module Cheese
{
  export module UIComponents
  {
    export var MutliSelectOption = React.createClass(
    {
      displayName: "MutliSelectOption",

      shouldComponentUpdate: function(newProps: any)
      {
        return this.props.isChecked !== newProps.isChecked;
      },
      render: function()
      {
        return(
          React.DOM.li(
          {
            className: "multi-select-option clearfix"
          },
            React.DOM.input(
            {
              className: "multi-select-option-checkbox",
              type: "checkbox",
              checked: this.props.isChecked,
              onChange: this.props.onSelect,
              id: this.props.key
            }),
            React.DOM.label(
            {
              className: "multi-select-option-label",
              htmlFor: this.props.key
            },
              this.props.label
            )
          )
        );
      }
    })
  }
}
