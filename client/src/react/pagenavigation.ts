module Cheese
{
  export module UIComponents
  {
    export var PageNavigation = React.createClass(
    {
      displayName: "PageNavigation",
      mixins: [React.addons.PureRenderMixin],

      componentDidMount: function()
      {

        document.body.addEventListener("keydown", function(e)
        {
          if (e.target.tagName === "INPUT" && e.target.type === "text")
          {
            return;
          }

          switch (e.keyCode)
          {
            case 39:
            {
              this.handleShiftPage(1, true);
              break;
            }
            case 37:
            {
              this.handleShiftPage(-1, true);
              break;
            }
          }
        }.bind(this));
      },

      handleSetPage: function(pageNumber: number)
      {
        this.props.onSetPage(pageNumber);
      },
      handleShiftPage: function(amount: number, shouldDelay: boolean = false)
      {
        this.props.onShiftPage(amount, shouldDelay);
      },

      handleExpand: function()
      {
        var pageToSet = parseInt(window.prompt(
          "Select page number " + 1 + "..." + this.props.totalPages));

        if (isFinite(pageToSet))
        {
          this.handleSetPage(pageToSet);
        }
      },

      makePageLink: function(pageNumber: number)
      {
        var className = "page-navigation-link page-navigation-number";

        if (this.props.currentPage === pageNumber)
        {
          className += " page-navigation-current";
        }

        return(
          React.DOM.li(
          {
            className: className,
            onClick: this.handleSetPage.bind(this, pageNumber),
            key: "" + pageNumber
          },
            pageNumber
          )
        );
      },

      makeExpandLink: function(key: number)
      {
        return(
          React.DOM.li(
          {
            className: "page-navigation-link page-navigation-expand",
            onClick: this.handleExpand,
            key: "expand" + key
          }, "...")
        );
      },

      makeNavigationLinks: function(maxElementsToShow: number = 11)
      {
        if (this.props.totalPages <= maxElementsToShow)
        {
          var allElements = [];
          for (var i = 1; i < this.props.totalPages + 1; i++)
          {
            allElements.push(this.makePageLink(i));
          }

          return allElements;
        }
        
        var currentPage = this.props.currentPage;
        
        var elementsLeftToPlace = maxElementsToShow - 1;
        var maxElementsPerSide = elementsLeftToPlace / 2;

        var possibleElementsFront = currentPage - 1;
        var possibleElementsBack = this.props.totalPages - currentPage;

        var leftoverFromFront = clamp(maxElementsPerSide - possibleElementsFront, 0, maxElementsPerSide);
        var leftoverFromBack = clamp(maxElementsPerSide - possibleElementsBack, 0, maxElementsPerSide);

        var hasFrontExpand = possibleElementsFront > maxElementsPerSide;
        var hasBackExpand = possibleElementsBack > maxElementsPerSide;

        var elementsFront = clamp(possibleElementsFront, 0, maxElementsPerSide - (hasFrontExpand ? 2 : 0) + leftoverFromBack);
        var elementsBack = clamp(possibleElementsBack, 0, maxElementsPerSide - (hasBackExpand ? 2 : 0) + leftoverFromFront);

        var allElements = [];

        if (hasFrontExpand)
        {
          allElements.push(this.makePageLink(1));
          allElements.push(this.makeExpandLink("front"));
        }

        for (var i = 0; i < elementsFront; i++)
        {
          allElements.push(this.makePageLink(currentPage - elementsFront + i));
        }

        allElements.push(this.makePageLink(currentPage));

        for (var i = 0; i < elementsBack; i++)
        {
          allElements.push(this.makePageLink(currentPage + i + 1));
        }

        if (hasBackExpand)
        {
          allElements.push(this.makeExpandLink("back"));
          allElements.push(this.makePageLink(this.props.totalPages));
        }

        return allElements;
      },

      render: function()
      {
        var pageLinks = this.makeNavigationLinks(11);

        var backArrowClass = "page-navigation-link page-navigation-back";
        var backArrowDisabled = false;
        if (this.props.currentPage === 1)
        {
          backArrowClass += " disabled";
          backArrowDisabled = true;
        }

        var frontArrowClass = "page-navigation-link page-navigation-front";
        var frontArrowDisabled = false;
        if (this.props.currentPage >= this.props.totalPages)
        {
          frontArrowClass += " disabled";
          frontArrowDisabled = true;
        }

        return(
          React.DOM.nav(
          {
            className: "page-navigation-container"
          },
            React.DOM.li(
            {
              className: backArrowClass,
              onClick: this.handleShiftPage.bind(this, -1, false),
              disabled: backArrowDisabled
            },
              "<"
            ),
            pageLinks,
            React.DOM.li(
            {
              className: frontArrowClass,
              onClick: this.handleShiftPage.bind(this, 1, false),
              disabled: frontArrowDisabled
            },
              ">"
            )
          )
        );
      }
    })
  }
}
