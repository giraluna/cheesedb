/// <reference path="loader.ts" />
/// <reference path="requester.ts" />


/// <reference path="react/site.ts" />

module Cheese
{
  export enum ReactScene
  {
    search
  }
  export class App
  {
    searchData: any;

    site: any;
    reactContainer: HTMLElement;
    currentScene: ReactScene;
    requester: Requester;

    constructor()
    {
      new Loader(this.initApp.bind(this));
    }
    initApp(loader: Loader)
    {
      this.searchData = loader.jsonData;
      this.requester = new Requester(1);
      this.initUI();
    }
    initUI()
    {
      this.reactContainer = document.getElementById("react-container");
      this.currentScene = ReactScene.search;
      this.render();
    }
    switchScene(newScene: ReactScene)
    {
      this.currentScene = newScene;
      this.render();
    }
    render()
    {
      this.site = React.renderComponent(
        UIComponents.Site(
        {
          currentScene: this.currentScene,
          searchData: this.searchData,
          requester: this.requester
        }),
        this.reactContainer
      );
    }
  }
}

var app = new Cheese.App();
