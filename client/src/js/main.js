var Cheese;
(function (Cheese) {
    var Requester = (function () {
        function Requester(// onSuccess: (any) => void,
        // onFailure: (any) => void,
        timesToRetry) {
            this.timesRetried = 0;
            // this.onSuccess = onSuccess;
            // this.onFailure = onFailure;
            this.timesToRetry = timesToRetry;
        }
        Requester.prototype.handleFailure = function (response) {
            if (this.timesRetried < this.timesToRetry) {
                this.makeSearchRequest(this.queryString, true);
            } else {
                this.onFailure(response);
            }
        };

        Requester.prototype.makeSearchRequest = function (queryString, isRetry) {
            if (typeof isRetry === "undefined") { isRetry = false; }
            var self = this;
            this.queryString = queryString;

            if (isRetry) {
                this.timesRetried++;
            } else {
                this.timesRetried = 0;
            }

            var request = new XMLHttpRequest();
            request.open("get", "/search?" + this.queryString, true);

            request.onload = function () {
                if (request.status >= 200 && request.status < 400) {
                    self.onSuccess(JSON.parse(request.response));
                } else {
                    self.handleFailure(request);
                }
            };

            request.onerror = this.handleFailure;
            request.send();
        };
        return Requester;
    })();
    Cheese.Requester = Requester;
})(Cheese || (Cheese = {}));
/// <reference path="requester.ts" />
var Cheese;
(function (Cheese) {
    var Loader = (function () {
        function Loader(onLoaded) {
            this.loaded = {
                DOM: false
            };
            this.onLoaded = onLoaded;
            this.loadDOM();
        }
        Loader.prototype.loadIfAllDone = function () {
            for (var prop in this.loaded) {
                if (!this.loaded[prop]) {
                    return false;
                }
            }

            this.jsonData = JSON.parse(document.getElementById("initial-search-data").innerHTML);

            this.onLoaded(this);
        };
        Loader.prototype.loadDOM = function () {
            var self = this;
            if (document.readyState === "interactive" || document.readyState === "complete") {
                self.loaded.DOM = true;
                self.loadIfAllDone();
            } else {
                document.addEventListener('DOMContentLoaded', function () {
                    self.loaded.DOM = true;
                    self.loadIfAllDone();
                });
            }
        };
        return Loader;
    })();
    Cheese.Loader = Loader;
})(Cheese || (Cheese = {}));
var Cheese;
(function (Cheese) {
    function clamp(val, min, max) {
        if (val < min)
            return min;
        else if (val > max)
            return max;
        else
            return val;
    }
    Cheese.clamp = clamp;
})(Cheese || (Cheese = {}));
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.SearchParameter = React.createClass({
            displayName: "SearchParameter",
            shouldComponentUpdate: function (newProps) {
                return newProps.value !== this.props.value;
            },
            handleClear: function () {
            },
            render: function () {
                return (React.DOM.div({
                    className: "search-parameter-container"
                }, React.DOM.div({
                    className: "search-parameter-label"
                }, this.props.label), this.props.children));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.MutliSelectOption = React.createClass({
            displayName: "MutliSelectOption",
            shouldComponentUpdate: function (newProps) {
                return this.props.isChecked !== newProps.isChecked;
            },
            render: function () {
                return (React.DOM.li({
                    className: "multi-select-option clearfix"
                }, React.DOM.input({
                    className: "multi-select-option-checkbox",
                    type: "checkbox",
                    checked: this.props.isChecked,
                    onChange: this.props.onSelect,
                    id: this.props.key
                }), React.DOM.label({
                    className: "multi-select-option-label",
                    htmlFor: this.props.key
                }, this.props.label)));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
/// <reference path="multiselectoption.ts" />
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.MultiSelect = React.createClass({
            displayName: "MultiSelect",
            getInitialState: function () {
                return ({
                    selected: [],
                    selectionMode: this.props.initialSelectionMode || "ANY",
                    availableModes: this.props.availableModes || ["ANY", "ALL", "NOT"],
                    displayString: ""
                });
            },
            getValuesFromString: function (source) {
                return (this.props.options.filter(function (value) {
                    return source.indexOf(value) !== -1;
                }));
            },
            changeSelectionMode: function (e) {
                this.setState({ selectionMode: e.target.value }, this.props.onChange);
            },
            setDisplayString: function (e) {
                this.setState({
                    displayString: e.target.value,
                    selected: this.getValuesFromString(e.target.value)
                }, this.props.onChange);
            },
            makeDisplayString: function (selected) {
                if (typeof selected === "undefined") { selected = this.state.selected; }
                return selected.join(", ");
            },
            getQueryString: function () {
                if (this.state.selected.length === 0) {
                    return null;
                } else if (this.state.selected.length === 1 && this.state.selectionMode !== "NOT") {
                    return this.state.selected[0];
                } else {
                    return this.state.selectionMode + "(" + this.makeDisplayString() + ")";
                }
            },
            toggleValue: function (value) {
                var index = this.state.selected.indexOf(value);
                var newSelected = this.state.selected.slice(0);

                if (index !== -1) {
                    newSelected.splice(index, 1);
                } else {
                    newSelected.push(value);
                }

                newSelected.sort();

                this.setState({
                    selected: newSelected,
                    displayString: this.makeDisplayString(newSelected)
                }, this.props.onChange);

                return false;
            },
            clearSelection: function () {
                this.setState({
                    selected: [],
                    displayString: ""
                }, this.props.onChange);
            },
            render: function () {
                var controller = null;
                if (this.props.hasController) {
                    var selectionModeOptions = [];

                    for (var i = 0; i < this.state.availableModes.length; i++) {
                        var mode = this.state.availableModes[i];
                        selectionModeOptions.push(React.DOM.option({
                            value: mode,
                            key: mode
                        }, mode));
                    }
                    controller = React.DOM.div({
                        className: "multi-select-controller"
                    }, React.DOM.select({
                        className: "multi-select-controller-mode",
                        value: this.state.selectionMode,
                        onChange: this.changeSelectionMode
                    }, selectionModeOptions), React.DOM.input({
                        className: "multi-select-controller-input",
                        value: this.state.displayString,
                        onChange: this.setDisplayString
                    }));
                }

                var options = [];
                for (var i = 0; i < this.props.options.length; i++) {
                    var option = this.props.options[i];
                    options.push(Cheese.UIComponents.MutliSelectOption({
                        onSelect: this.toggleValue.bind(this, option),
                        key: option,
                        label: option,
                        isChecked: this.state.selected.indexOf(option) !== -1
                    }));
                }

                return (React.DOM.div({
                    className: "multi-select"
                }, controller, React.DOM.ol({
                    className: "multi-select-options"
                }, options)));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
/// <reference path="searchparameter.ts" />
/// <reference path="multiselect.ts" />
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.SearchParameters = React.createClass({
            displayName: "SearchParameters",
            getInitialState: function () {
                return ({
                    animal: [],
                    country: null,
                    name: null,
                    hardness: [],
                    animalString: "",
                    hardnessString: ""
                });
            },
            getSelectedValues: function (selectElement) {
                var values = [];
                for (var i = 0; i < selectElement.selectedOptions.length; i++) {
                    var value = selectElement.selectedOptions[i].value;
                    if (value)
                        values.push(value);
                }

                return values;
            },
            makeMultiSelectQueryString: function (values, prefix) {
                var queryString = "";

                if (values && values.length > 0) {
                    if (values.length === 1) {
                        queryString += values[0];
                    } else {
                        queryString += prefix + values.join(",") + ")";
                    }
                }

                return queryString;
            },
            handleNameChange: function (e) {
                this.setState({ name: e.target.value }, this.props.delayedOnUpdate);
            },
            handleCountryChange: function (e) {
                this.setState({ country: e.target.value }, this.props.delayedOnUpdate);
            },
            handleAnimalChange: function () {
                var animalSelect = this.refs["animalSelect"];

                this.setState({
                    animal: animalSelect.state.selected,
                    animalString: animalSelect.getQueryString()
                }, this.props.onUpdate);
            },
            handleHardnessChange: function () {
                var hardnessSelect = this.refs["hardnessSelect"];

                this.setState({
                    hardness: hardnessSelect.state.selected,
                    hardnessString: hardnessSelect.getQueryString()
                }, this.props.onUpdate);
            },
            render: function () {
                return (React.DOM.div({
                    className: "search-parameters"
                }, Cheese.UIComponents.SearchParameter({
                    label: "Name"
                }, React.DOM.input({
                    className: "search-parameter-input",
                    value: this.state.name,
                    placeholder: "Cheese name",
                    onChange: this.handleNameChange
                })), Cheese.UIComponents.SearchParameter({
                    label: "Origin country"
                }, React.DOM.input({
                    className: "search-parameter-input",
                    value: this.state.country,
                    placeholder: "Origin country",
                    onChange: this.handleCountryChange
                })), Cheese.UIComponents.SearchParameter({
                    label: "Source(s) of milk"
                }, Cheese.UIComponents.MultiSelect({
                    onChange: this.handleAnimalChange,
                    ref: "animalSelect",
                    hasController: true,
                    initialSelectionMode: "ALL",
                    options: [
                        "cow", "sheep", "goat", "water buffalo",
                        "yak", "camel", "donkey", "moose"]
                })), Cheese.UIComponents.SearchParameter({
                    label: "Texture"
                }, Cheese.UIComponents.MultiSelect({
                    onChange: this.handleHardnessChange,
                    ref: "hardnessSelect",
                    initialSelectionMode: "ANY",
                    availableModes: ["ANY", "NOT"],
                    options: ["soft", "semi-soft", "firm", "semi-hard", "hard"]
                }))));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.List = React.createClass({
            displayName: "List",
            mixins: [React.addons.PureRenderMixin],
            getInitialState: function () {
                var initialColumn = this.props.initialSortOrder ? this.props.initialSortOrder[0] : this.props.initialColumns[0];

                var initialSelected = this.props.listItems[0];

                return ({
                    columns: this.props.initialColumns,
                    selected: initialSelected,
                    selectedColumn: initialColumn,
                    sortingOrder: this.makeInitialSortingOrder(this.props.initialColumns, initialColumn)
                });
            },
            componentDidMount: function () {
                var self = this;

                if (!this.props.preventAutoResize) {
                    window.addEventListener("resize", this.setDesiredHeight, false);
                }

                if (this.props.selectable) {
                    this.getDOMNode().addEventListener("keydown", function (event) {
                        switch (event.keyCode) {
                            case 40: {
                                self.shiftSelection(1);
                                break;
                            }
                            case 38: {
                                self.shiftSelection(-1);
                                break;
                            }
                            default: {
                                return;
                            }
                        }
                    });
                }

                if (this.props.autoSelect) {
                    this.handleSelectRow(this.props.sortedItems[0]);
                    this.getDOMNode().focus();
                }
            },
            componentWillUnmount: function () {
                window.removeEventListener("resize", this.setDesiredHeight);
            },
            componentDidUpdate: function () {
                if (!this.props.preventAutoResize) {
                    this.setDesiredHeight();
                }
            },
            setDesiredHeight: function () {
                var ownNode = this.getDOMNode();
                var innerNode = this.refs.inner.getDOMNode();

                ownNode.style.height = "auto";
                innerNode.style.height = "auto";

                var parentHeight = ownNode.parentNode.getBoundingClientRect().height;
                var ownRect = ownNode.getBoundingClientRect();
                var ownHeight = ownRect.height;

                var strippedOwnHeight = parseInt(getComputedStyle(ownNode).height);
                var extraHeight = ownHeight - strippedOwnHeight;

                var desiredHeight = parentHeight - extraHeight;

                var maxHeight = window.innerHeight - ownRect.top - extraHeight;

                desiredHeight = Math.min(desiredHeight, maxHeight);

                ownNode.style.height = "" + desiredHeight + "px";
                innerNode.style.height = "" + desiredHeight + "px";
            },
            handleScroll: function (e) {
                // scrolls header to match list contents
                var header = this.refs.header.getDOMNode();
                var titles = header.getElementsByClassName("fixed-table-th-inner");

                var marginString = "-" + e.target.scrollLeft + "px";

                for (var i = 0; i < titles.length; i++) {
                    titles[i].style.marginLeft = marginString;
                }
            },
            makeInitialSortingOrder: function (columns, initialColumn) {
                var initialSortOrder = this.props.initialSortOrder;
                if (!initialSortOrder || initialSortOrder.length < 1) {
                    initialSortOrder = [initialColumn];
                }

                var order = initialSortOrder;

                for (var i = 0; i < columns.length; i++) {
                    if (!columns[i].order) {
                        columns[i].order = columns[i].defaultOrder;
                    }
                    if (initialSortOrder.indexOf(columns[i]) < 0) {
                        order.push(columns[i]);
                    }
                }

                return order;
            },
            getNewSortingOrder: function (newColumn) {
                var order = this.state.sortingOrder.slice(0);
                var current = order.indexOf(newColumn);

                if (current >= 0) {
                    order.splice(current);
                }

                order.unshift(newColumn);

                return order;
            },
            handleSelectColumn: function (column) {
                if (column.notSortable)
                    return;

                this.orderSelectedColumn(column);

                if (this.props.onSelectColumn) {
                    this.preventRender = true;
                    this.props.onSelectColumn(column, this.onSelectColumn.bind(this, column));
                } else {
                    this.onSelectColumn();
                }
            },
            orderSelectedColumn: function (column) {
                function getReverseOrder(order) {
                    return order === "desc" ? "asc" : "desc";
                }

                if (this.state.selectedColumn.key === column.key) {
                    column.order = getReverseOrder(column.order);
                } else {
                    column.order = column.defaultOrder;
                }
            },
            onSelectColumn: function (column) {
                this.preventRender = false;
                if (this.state.selectedColumn.key !== column.key) {
                    this.setState({
                        selectedColumn: column,
                        sortingOrder: this.getNewSortingOrder(column)
                    });
                } else {
                    this.forceUpdate();
                }
            },
            handleSelectRow: function (row) {
                if (this.props.onRowChange && row)
                    this.props.onRowChange.call(null, row);

                this.setState({
                    selected: row
                });
            },
            sort: function () {
                var itemsToSort = this.props.listItems;
                var columnsToTry = this.state.columns;
                var sortOrder = this.state.sortingOrder;
                var sortFunctions = {};

                function makeSortingFunction(column) {
                    if (column.sortingFunction)
                        return column.sortingFunction;

                    var propToSortBy = column.propToSortBy || column.key;

                    return (function (a, b) {
                        var a1 = a.data[propToSortBy];
                        var b1 = b.data[propToSortBy];

                        if (a1 > b1)
                            return 1;
                        else if (a1 < b1)
                            return -1;
                        else
                            return 0;
                    });
                }

                itemsToSort.sort(function (a, b) {
                    var result = 0;
                    for (var i = 0; i < sortOrder.length; i++) {
                        var columnToSortBy = sortOrder[i];

                        if (!sortFunctions[columnToSortBy.key]) {
                            sortFunctions[columnToSortBy.key] = makeSortingFunction(columnToSortBy);
                        }
                        var sortFunction = sortFunctions[columnToSortBy.key];

                        result = sortFunction(a, b);

                        if (columnToSortBy.order === "desc") {
                            result *= -1;
                        }

                        if (result)
                            return result;
                    }

                    return 0;
                });

                this.props.sortedItems = itemsToSort;
            },
            shiftSelection: function (amountToShift) {
                var reverseIndexes = {};
                for (var i = 0; i < this.props.sortedItems.length; i++) {
                    reverseIndexes[this.props.sortedItems[i].key] = i;
                }
                ;
                var currSelectedIndex = reverseIndexes[this.state.selected.key];
                var nextIndex = (currSelectedIndex + amountToShift) % this.props.sortedItems.length;
                if (nextIndex < 0) {
                    nextIndex += this.props.sortedItems.length;
                }

                this.handleSelectRow(this.props.sortedItems[nextIndex]);
            },
            render: function () {
                var self = this;
                var columns = [];
                var headerLabels = [];

                this.state.columns.forEach(function (column) {
                    var colProps = {
                        key: column.key
                    };

                    if (self.props.colStylingFN) {
                        colProps = self.props.colStylingFN(column, colProps);
                    }

                    columns.push(React.DOM.col(colProps));

                    var sortStatus = null;

                    if (!column.notSortable)
                        sortStatus = " sortable";

                    if (self.state.selectedColumn.key === column.key) {
                        sortStatus += " sorted-" + column.order;
                    } else if (!column.notSortable)
                        sortStatus += " unsorted";

                    headerLabels.push(React.DOM.th({
                        key: column.key
                    }, React.DOM.div({
                        className: "fixed-table-th-inner"
                    }, React.DOM.div({
                        className: "fixed-table-th-content" + sortStatus,
                        title: column.title || colProps.title || null,
                        onMouseDown: self.handleSelectColumn.bind(null, column),
                        onTouchStart: self.handleSelectColumn.bind(null, column)
                    }, column.label))));
                });

                this.sort();

                var sortedItems = this.props.sortedItems;

                var rows = [];

                sortedItems.forEach(function (item) {
                    item.data.key = item.key;
                    item.data.activeColumns = self.state.columns;
                    item.data.handleClick = self.handleSelectRow.bind(null, item);
                    var row = item.data.rowConstructor(item.data);

                    rows.push(row);
                });

                var tabIndex = null;
                if (this.props.selectable) {
                    tabIndex = isFinite(this.props.tabIndex) ? this.props.tabIndex : 1;
                }

                return (React.DOM.div({
                    className: "fixed-table-container" + (this.props.noHeader ? " no-header" : ""),
                    tabIndex: tabIndex
                }, React.DOM.div({ className: "fixed-table-header-background" }), React.DOM.div({
                    className: "fixed-table-container-inner",
                    ref: "inner",
                    onScroll: this.handleScroll
                }, React.DOM.table({
                    className: "react-list"
                }, React.DOM.colgroup(null, columns), React.DOM.thead({ className: "fixed-table-actual-header", ref: "header" }, React.DOM.tr(null, headerLabels)), React.DOM.thead({ className: "fixed-table-hidden-header" }, React.DOM.tr(null, headerLabels)), React.DOM.tbody(null, rows)))));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.CheeseListitem = React.createClass({
            displayName: "CheeseListitem",
            shouldComponentUpdate: function (newProps) {
                return this.props.url !== newProps.url;
            },
            makeCell: function (type) {
                var className = "cheese-list-item-cell" + " cheese-list-item-" + type;
                var content;
                switch (type) {
                    case "name": {
                        content = React.DOM.a({
                            target: "_blank",
                            href: this.props.url
                        }, this.props[type]);
                        break;
                    }
                    case "animal": {
                        content = this.props[type].join(", ");
                        break;
                    }
                    default: {
                        content = this.props[type];
                        break;
                    }
                }

                return (React.DOM.td({
                    key: type,
                    className: className,
                    title: this.props[type],
                    "data-column": type
                }, content));
            },
            render: function () {
                var columns = this.props.activeColumns;

                var cells = [];

                for (var i = 0; i < columns.length; i++) {
                    var cell = this.makeCell(columns[i].key);
                    cells.push(cell);
                }

                return (React.DOM.tr({ className: "cheese-list-item" }, cells));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
/// <reference path="list.ts" />
/// <reference path="cheeselistitem.ts" />
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        var enumHardness;
        (function (enumHardness) {
            enumHardness[enumHardness["soft"] = 0] = "soft";
            enumHardness[enumHardness["semi-soft"] = 1] = "semi-soft";
            enumHardness[enumHardness["firm"] = 2] = "firm";
            enumHardness[enumHardness["semi-hard"] = 3] = "semi-hard";
            enumHardness[enumHardness["hard"] = 4] = "hard";
            enumHardness[enumHardness["null"] = 5] = "null";
        })(enumHardness || (enumHardness = {}));
        UIComponents.CheeseList = React.createClass({
            displayName: "CheeseList",
            getInitialState: function () {
                return ({
                    hasMobileLinks: false
                });
            },
            componentDidMount: function () {
                window.addEventListener("resize", this.handleResize, true);
            },
            componentWillUnmount: function () {
                window.removeEventListener("resize", this.handleResize);
            },
            handleResize: function (e) {
                if (window.innerWidth <= 680) {
                    this.setState({ hasMobileLinks: true });
                } else if (window.innerWidth > 680) {
                    this.setState({ hasMobileLinks: false });
                }

                this.lastWidth = window.innerWidth;
            },
            render: function () {
                var urlPrefix = (this.state.hasMobileLinks ? "m." : "");
                var rows = [];
                for (var i = 0; i < this.props.cheeseData.length; i++) {
                    var cheese = this.props.cheeseData[i];
                    var data = {
                        name: cheese.name,
                        url: "http://en." + urlPrefix + "wikipedia.org/wiki/" + cheese.name,
                        country: cheese.country,
                        animal: cheese.animals,
                        animalsSort: (cheese.animals.length === 0 ? ["zzz"] : cheese.animals),
                        hardness: cheese.hardness,
                        enumHardness: enumHardness[cheese.hardness],
                        rowConstructor: Cheese.UIComponents.CheeseListitem
                    };

                    rows.push({
                        key: cheese.name,
                        data: data
                    });
                }

                var columns = [
                    {
                        label: "Name",
                        key: "name",
                        defaultOrder: "asc"
                    },
                    {
                        label: "Country",
                        key: "country",
                        defaultOrder: "asc"
                    },
                    {
                        label: "Milk",
                        key: "animal",
                        defaultOrder: "asc",
                        propToSortBy: "animalsSort"
                    },
                    {
                        label: "Texture",
                        key: "hardness",
                        defaultOrder: "asc",
                        propToSortBy: "enumHardness"
                    }
                ];

                return (React.DOM.div({
                    className: "cheese-list"
                }, Cheese.UIComponents.List({
                    listItems: rows,
                    initialColumns: columns,
                    onSelectColumn: this.props.onSelectColumn,
                    preventAutoResize: true
                })));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.PageNavigation = React.createClass({
            displayName: "PageNavigation",
            mixins: [React.addons.PureRenderMixin],
            componentDidMount: function () {
                document.body.addEventListener("keydown", function (e) {
                    if (e.target.tagName === "INPUT" && e.target.type === "text") {
                        return;
                    }

                    switch (e.keyCode) {
                        case 39: {
                            this.handleShiftPage(1, true);
                            break;
                        }
                        case 37: {
                            this.handleShiftPage(-1, true);
                            break;
                        }
                    }
                }.bind(this));
            },
            handleSetPage: function (pageNumber) {
                this.props.onSetPage(pageNumber);
            },
            handleShiftPage: function (amount, shouldDelay) {
                if (typeof shouldDelay === "undefined") { shouldDelay = false; }
                this.props.onShiftPage(amount, shouldDelay);
            },
            handleExpand: function () {
                var pageToSet = parseInt(window.prompt("Select page number " + 1 + "..." + this.props.totalPages));

                if (isFinite(pageToSet)) {
                    this.handleSetPage(pageToSet);
                }
            },
            makePageLink: function (pageNumber) {
                var className = "page-navigation-link page-navigation-number";

                if (this.props.currentPage === pageNumber) {
                    className += " page-navigation-current";
                }

                return (React.DOM.li({
                    className: className,
                    onClick: this.handleSetPage.bind(this, pageNumber),
                    key: "" + pageNumber
                }, pageNumber));
            },
            makeExpandLink: function (key) {
                return (React.DOM.li({
                    className: "page-navigation-link page-navigation-expand",
                    onClick: this.handleExpand,
                    key: "expand" + key
                }, "..."));
            },
            makeNavigationLinks: function (maxElementsToShow) {
                if (typeof maxElementsToShow === "undefined") { maxElementsToShow = 11; }
                if (this.props.totalPages <= maxElementsToShow) {
                    var allElements = [];
                    for (var i = 1; i < this.props.totalPages + 1; i++) {
                        allElements.push(this.makePageLink(i));
                    }

                    return allElements;
                }

                var currentPage = this.props.currentPage;

                var elementsLeftToPlace = maxElementsToShow - 1;
                var maxElementsPerSide = elementsLeftToPlace / 2;

                var possibleElementsFront = currentPage - 1;
                var possibleElementsBack = this.props.totalPages - currentPage;

                var leftoverFromFront = Cheese.clamp(maxElementsPerSide - possibleElementsFront, 0, maxElementsPerSide);
                var leftoverFromBack = Cheese.clamp(maxElementsPerSide - possibleElementsBack, 0, maxElementsPerSide);

                var hasFrontExpand = possibleElementsFront > maxElementsPerSide;
                var hasBackExpand = possibleElementsBack > maxElementsPerSide;

                var elementsFront = Cheese.clamp(possibleElementsFront, 0, maxElementsPerSide - (hasFrontExpand ? 2 : 0) + leftoverFromBack);
                var elementsBack = Cheese.clamp(possibleElementsBack, 0, maxElementsPerSide - (hasBackExpand ? 2 : 0) + leftoverFromFront);

                var allElements = [];

                if (hasFrontExpand) {
                    allElements.push(this.makePageLink(1));
                    allElements.push(this.makeExpandLink("front"));
                }

                for (var i = 0; i < elementsFront; i++) {
                    allElements.push(this.makePageLink(currentPage - elementsFront + i));
                }

                allElements.push(this.makePageLink(currentPage));

                for (var i = 0; i < elementsBack; i++) {
                    allElements.push(this.makePageLink(currentPage + i + 1));
                }

                if (hasBackExpand) {
                    allElements.push(this.makeExpandLink("back"));
                    allElements.push(this.makePageLink(this.props.totalPages));
                }

                return allElements;
            },
            render: function () {
                var pageLinks = this.makeNavigationLinks(11);

                var backArrowClass = "page-navigation-link page-navigation-back";
                var backArrowDisabled = false;
                if (this.props.currentPage === 1) {
                    backArrowClass += " disabled";
                    backArrowDisabled = true;
                }

                var frontArrowClass = "page-navigation-link page-navigation-front";
                var frontArrowDisabled = false;
                if (this.props.currentPage >= this.props.totalPages) {
                    frontArrowClass += " disabled";
                    frontArrowDisabled = true;
                }

                return (React.DOM.nav({
                    className: "page-navigation-container"
                }, React.DOM.li({
                    className: backArrowClass,
                    onClick: this.handleShiftPage.bind(this, -1, false),
                    disabled: backArrowDisabled
                }, "<"), pageLinks, React.DOM.li({
                    className: frontArrowClass,
                    onClick: this.handleShiftPage.bind(this, 1, false),
                    disabled: frontArrowDisabled
                }, ">")));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
/// <reference path="../utility.ts" />
/// <reference path="searchparameters.ts" />
/// <reference path="cheeselist.ts" />
/// <reference path="pagenavigation.ts" />
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.Search = React.createClass({
            displayName: "Search",
            mixins: [React.addons.PureRenderMixin],
            componentDidMount: function () {
                this.props.requester.onSuccess = this.handleSearchSuccess;
                this.props.requester.onFailure = this.handleSearchFailure;

                this.delayedCallbacks = {};
            },
            getInitialState: function () {
                return ({
                    currentPage: 1,
                    offset: 0,
                    totalPages: Math.ceil(this.props.searchData.matchCount / 20),
                    searchData: this.props.searchData,
                    itemsPerPage: 20,
                    isFetching: false
                });
            },
            componentWillUnmount: function () {
                for (var tag in this.delayedCallbacks) {
                    window.clearTimeout(this.delayedCallbacks[tag]);
                    this.delayedCallbacks[tag] = null;
                }

                this.props.requester.onSuccess = null;
                this.props.requester.onFailure = null;
            },
            makeQueryString: function () {
                var queryString = "";
                queryString += "limit=" + this.state.itemsPerPage;
                queryString += "&offset=" + this.state.offset;

                var params = this.refs.params.state;

                ["animal", "hardness"].forEach(function (param) {
                    if (params[param] && params[param].length > 0) {
                        var paramString = params[param + "String"];
                        var q = param === "hardness" ? "\"" : "";

                        queryString += "&" + param + "=" + q + paramString + q;
                    }
                });

                ["name", "country"].forEach(function (param) {
                    if (params[param]) {
                        queryString += "&" + param + "=" + params[param];
                    }
                });

                if (this.state.sortBy) {
                    queryString += "&sortBy=" + this.state.sortBy;
                }
                if (this.state.order) {
                    queryString += "&order=" + this.state.order;
                }

                return queryString;
            },
            handleParameterUpdate: function (resetPosition) {
                if (typeof resetPosition === "undefined") { resetPosition = false; }
                if (resetPosition) {
                    this.shouldResetPosition = true;
                }

                if (this.shouldResetPosition) {
                    this.shouldResetPosition = false;
                    this.setState({
                        offset: 0,
                        currentPage: 1
                    }, this.executeSearch);
                } else {
                    this.executeSearch();
                }
            },
            executeDelayed: function (tag, delay, callback) {
                if (this.delayedCallbacks[tag]) {
                    window.clearTimeout(this.delayedCallbacks[tag]);
                }

                this.delayedCallbacks[tag] = window.setTimeout(function () {
                    callback();
                    this.delayedCallbacks[tag] = null;
                }.bind(this), delay);
            },
            handleDelayedUpdate: function (delay) {
                if (typeof delay === "undefined") { delay = 500; }
                this.executeDelayed("search", delay, this.handleParameterUpdate.bind(this, true));
            },
            executeSearch: function () {
                var newQueryString = this.makeQueryString();

                if (this.lastQueryString === newQueryString)
                    return;

                this.props.requester.makeSearchRequest(newQueryString);

                this.lastQueryString = newQueryString;
                this.setState({
                    isFetching: true
                });
            },
            handleSearchSuccess: function (searchData) {
                this.setState({
                    searchData: searchData,
                    totalPages: Math.ceil(searchData.matchCount / this.state.itemsPerPage),
                    isFetching: false
                }, this.afterSuccessfulSearch);
            },
            afterSuccessfulSearch: function () {
                if (this.afterSearchCallback) {
                    this.afterSearchCallback();
                    this.afterSearchCallback = null;
                }
            },
            handleSearchFailure: function (errorData) {
                console.error("Search failed");
                console.error(errorData);

                this.setState({ isFetching: false });
            },
            handleSetPage: function (newPage, shouldDelay) {
                if (typeof shouldDelay === "undefined") { shouldDelay = false; }
                if (this.state.currentPage === newPage)
                    return;

                newPage = Cheese.clamp(newPage, 1, this.state.totalPages);

                var callback = shouldDelay ? this.handleDelayedUpdate : this.handleParameterUpdate;

                this.setState({
                    currentPage: newPage,
                    offset: (newPage - 1) * this.state.itemsPerPage
                }, callback);
            },
            handleShiftPage: function (amount, shouldDelay) {
                if (typeof shouldDelay === "undefined") { shouldDelay = false; }
                this.handleSetPage(this.state.currentPage + amount, shouldDelay);
            },
            handleSetLimitPerPage: function (e) {
                var amount = Cheese.clamp(e.target.value, 10, 100);

                this.setState({
                    itemsPerPage: amount,
                    totalPages: Math.ceil(this.state.searchData.matchCount / amount),
                    currentPage: (this.state.offset === 0 ? 1 : 1 + Math.ceil(this.state.offset / amount))
                }, this.handleParameterUpdate);
            },
            handleSelectColumn: function (column, callback) {
                if (this.state.totalPages < 2) {
                    callback();
                } else {
                    this.afterSearchCallback = callback;

                    this.setState({ sortBy: column.key, order: column.order }, this.handleParameterUpdate.bind(this, true));
                }
            },
            render: function () {
                return (React.DOM.div({
                    className: "search-container"
                }, Cheese.UIComponents.SearchParameters({
                    ref: "params",
                    onUpdate: this.handleParameterUpdate.bind(this, true),
                    delayedOnUpdate: this.handleDelayedUpdate
                }), React.DOM.div({
                    className: "search-contents"
                }, React.DOM.div({
                    className: "search-results-wrapper" + (this.state.isFetching ? " fetching" : "")
                }, Cheese.UIComponents.CheeseList({
                    cheeseData: this.state.searchData.cheeses,
                    onSelectColumn: this.handleSelectColumn
                }), this.state.isFetching ? React.DOM.div({
                    className: "search-results-fetching-wrapper"
                }, React.DOM.img({
                    className: "search-results-fetching-image",
                    src: "img\/ajax-loader.gif"
                })) : null), React.DOM.div({
                    className: "search-navigation-wrapper"
                }, React.DOM.div({
                    className: "search-navigation-inner"
                }, Cheese.UIComponents.PageNavigation({
                    currentPage: this.state.currentPage,
                    totalPages: this.state.totalPages,
                    onSetPage: this.handleSetPage,
                    onShiftPage: this.handleShiftPage
                }), React.DOM.div({
                    className: "search-navigation-per-page-container"
                }, React.DOM.label({
                    className: "search-navigation-per-page-label",
                    htmlFor: "per-page-select"
                }, "Results per page:"), React.DOM.select({
                    id: "per-page-select",
                    value: this.state.itemsPerPage,
                    onChange: this.handleSetLimitPerPage
                }, React.DOM.option({ value: 10 }, 10), React.DOM.option({ value: 20 }, 20), React.DOM.option({ value: 40 }, 40), React.DOM.option({ value: 100 }, 100))))))));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
/// <reference path="../../lib/react.d.ts" />
/// <reference path="search.ts" />
var Cheese;
(function (Cheese) {
    (function (UIComponents) {
        UIComponents.Site = React.createClass({
            displayName: "Site",
            render: function () {
                var componentToRender;
                switch (this.props.currentScene) {
                    case 0 /* search */: {
                        componentToRender = Cheese.UIComponents.Search({
                            searchData: this.props.searchData,
                            requester: this.props.requester
                        });
                        break;
                    }
                }

                return (React.DOM.div({
                    className: "site"
                }, componentToRender));
            }
        });
    })(Cheese.UIComponents || (Cheese.UIComponents = {}));
    var UIComponents = Cheese.UIComponents;
})(Cheese || (Cheese = {}));
/// <reference path="loader.ts" />
/// <reference path="requester.ts" />
/// <reference path="react/site.ts" />
var Cheese;
(function (Cheese) {
    (function (ReactScene) {
        ReactScene[ReactScene["search"] = 0] = "search";
    })(Cheese.ReactScene || (Cheese.ReactScene = {}));
    var ReactScene = Cheese.ReactScene;
    var App = (function () {
        function App() {
            new Cheese.Loader(this.initApp.bind(this));
        }
        App.prototype.initApp = function (loader) {
            this.searchData = loader.jsonData;
            this.requester = new Cheese.Requester(1);
            this.initUI();
        };
        App.prototype.initUI = function () {
            this.reactContainer = document.getElementById("react-container");
            this.currentScene = 0 /* search */;
            this.render();
        };
        App.prototype.switchScene = function (newScene) {
            this.currentScene = newScene;
            this.render();
        };
        App.prototype.render = function () {
            this.site = React.renderComponent(Cheese.UIComponents.Site({
                currentScene: this.currentScene,
                searchData: this.searchData,
                requester: this.requester
            }), this.reactContainer);
        };
        return App;
    })();
    Cheese.App = App;
})(Cheese || (Cheese = {}));

var app = new Cheese.App();
//# sourceMappingURL=main.js.map
