/// <reference path="../../lib/react.d.ts" />
declare module Cheese {
    interface ICheeseData {
    }
    class Requester {
        public onSuccess: (any: any) => void;
        public onFailure: (any: any) => void;
        public timesToRetry: number;
        public timesRetried: number;
        public queryString: string;
        constructor(timesToRetry: number);
        public handleFailure(response: any): void;
        public makeSearchRequest(queryString: string, isRetry?: boolean): void;
    }
}
declare module Cheese {
    class Loader {
        public loaded: {
            DOM: boolean;
        };
        public jsonData: any;
        public onLoaded: (Loader: any) => void;
        constructor(onLoaded: (Loader: any) => void);
        public loadIfAllDone(): boolean;
        public loadDOM(): void;
    }
}
declare module Cheese {
    function clamp(val: number, min: number, max: number): number;
}
declare module Cheese {
    module UIComponents {
        var SearchParameter: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var MutliSelectOption: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var MultiSelect: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var SearchParameters: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var List: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var CheeseListitem: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var CheeseList: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var PageNavigation: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var Search: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    module UIComponents {
        var Site: React.ReactComponentFactory<{}, React.ReactComponent<{}, {}>>;
    }
}
declare module Cheese {
    enum ReactScene {
        search = 0,
    }
    class App {
        public searchData: any;
        public site: any;
        public reactContainer: HTMLElement;
        public currentScene: ReactScene;
        public requester: Cheese.Requester;
        constructor();
        public initApp(loader: Cheese.Loader): void;
        public initUI(): void;
        public switchScene(newScene: ReactScene): void;
        public render(): void;
    }
}
declare var app: Cheese.App;
