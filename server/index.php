<?php
  require_once "renderindexpage.php";
  require_once "searchfetcher.php";
  
  function getInitialRender($jsonData)
  {
    // nasty string escape to pass json to our node script as an argument
    $jsonData = "'" . str_replace("'", "'\''", $jsonData) . "'";
  
    $resultHTML;
    exec("node initialrender.js " . $jsonData, $resultHTML);

    return $resultHTML[0];
  }

  $searchFetcher = new SearchFetcher("returnError");

  $searchData = json_encode($searchFetcher->fetchResults());

  echo renderIndexPage($searchData, getInitialRender($searchData));
?>
