<?php
  function renderIndexPage($searchData, $renderedComponent)
  {
    return <<< HTML
      <!doctype html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <title>cheese</title>

        <link rel="stylesheet" type="text/css" href="lib/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

        <script src="lib/react-with-addons-0.11.0.js"></script>

        <script src="src/js/main.js"></script>
        
        <script id="initial-search-data" type="application/json">
          $searchData
        </script>

      </head>
      <body>
        <noscript class="no-javascript-notice">
          This site requires JavaScript to be enabled.
        </noscript>
        <div id="react-container">
          $renderedComponent
        </div>
        <div class="footer" style="width:100%;clear:both;padding-top:1em;">
          <div class="footer-inner" style="display:table;text-align:center;margin:0 auto;font-size:0.8em;">
            <span>
              Created for learning purposes only.
                <a href="https://bitbucket.org/giraluna/cheesedb">Source code</a>
              |
                <a href="api-docs">API documentation</a>
            </span>
            </div>
        </div>
      </body>
HTML;
  }
?>
