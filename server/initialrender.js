var React = require("../client/lib/react-with-addons-0.11.0.js");
var UIComponents = require("../client/src/react/js/reactmodule.js");

var fs = require("fs");

try
{
  var jsonData = process.argv[2];

  var rendered = React.renderComponentToString(UIComponents.Site(
  {
    currentScene: 0,
    searchData: JSON.parse(jsonData),
    requester: null
  }));

  process.stdout.write(rendered);
}
catch(e)
{
  process.stderr.write("An error occured while trying to render the page.", e);
}
