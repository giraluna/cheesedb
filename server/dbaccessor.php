<?php
  require_once "dbconnector.php";

  $dbh = getConnection();

  $statement = $dbh->prepare("
    SELECT *
    FROM cheeses
    LIMIT 20
    OFFSET :offset
  ");

  $offset = 20;
  $statement->bindParam(":offset", $offset, PDO::PARAM_INT);

  $statement->execute();

  $result = $statement->fetchAll();

  echo "<pre>";
  foreach($result as $row)
  {
    echo $row["name"]."\n";
  }
  echo "</pre>";

  $dbh = null;
?>
