<?php
  require_once "getconnection.php";
  require_once "unidecode.php";
  require_once "getsearchparameters.php";

  class SearchFetcher
  {
    private $onFailure;
    private $queryParams;

    public function __construct($onFailure = null)
    {
      if ($onFailure)
      {
        $this->onFailure = $onFailure;
      }
      else
      {
        $this->onFailure = $this->ignoreError;
      }
    }

    private function ignoreError($errormessage)
    {

    }

    private function buildStringQuery($key, $selectionGroups, &$paramIdGenerator, $searchInArray=False)
    {
      if (!$selectionGroups || count($selectionGroups) === 0)
      {
        return NULL;
      }

      $queryStrings = [];
      $paramsToBind = [];

      foreach($selectionGroups as $selector=>$selectionGroup)
      {
        $prefix = ($selector === "NOT" ? " NOT " : " ");
        $delimiter = ($selector === "ANY" ? " OR " : " AND ");

        $selectionStrings = [];
        foreach($selectionGroup as $param)
        {
          $normalized = Unidecode::decode($param);
          $paramIdGenerator++;
          $boundParamString = ":stringParam$paramIdGenerator";

          if ($searchInArray)
          {
            if ($selector === "NOT")
            {
              array_push($selectionStrings, "$boundParamString != ALL($key)");
            }
            else
            {
              array_push($selectionStrings, "$boundParamString = ANY($key)");
            }
            $paramsToBind[$boundParamString] = $normalized;
          }
          else
          {
            array_push($selectionStrings, "$key" . $prefix . "ILIKE $boundParamString");
            preg_match('/"([^"]+)"/', $normalized, $quotedString);
            if ($quotedString)
            {
              $paramsToBind[$boundParamString] = $quotedString[1];
            }
            else
            {
              $paramsToBind[$boundParamString] = "%$normalized%";
            }
          }
          
        }

        array_push($queryStrings, "(" . implode($delimiter, $selectionStrings) . ")");
      }

      return(
      [
        "string" => "(" . implode(" AND ", $queryStrings) . ")",
        "paramsToBind" => $paramsToBind
      ]);
    }

    private function buildFullSelectionQuery($params)
    {
      $selectionStrings = [];
      $paramsToBind = [];
      $paramIdGenerator = 0;

      foreach(["ascii_name", "country", "hardness"] as $param)
      {
        $stringQuery = $this->buildStringQuery($param, $params[$param], $paramIdGenerator);
        if ($stringQuery)
        {
          array_push($selectionStrings, $stringQuery["string"]);
          $paramsToBind = $paramsToBind + $stringQuery["paramsToBind"];
        }
      }

      $arrayQuery = $this->buildStringQuery("animal", $params["animal"], $paramIdGenerator, True);
      if ($arrayQuery)
      {
        array_push($selectionStrings, $arrayQuery["string"]);
        $paramsToBind = $paramsToBind + $arrayQuery["paramsToBind"];
      }

      $selectionQuery;
      if (count($selectionStrings) === 0)
      {
        $selectionQuery = NULL;
      }
      else
      {
        $selectionQuery = "WHERE (" . implode($selectionStrings, " AND ") . ")";
      }

      return(
      [
        "string" => $selectionQuery,
        "paramsToBind" => $paramsToBind
      ]);
    }

    private function cleanUpSearchResult(&$result)
    {
      foreach($result as $key=>$line)
      {
        $exploded = explode(",", $line["animals"]);

        if (strlen($exploded[0]) === 0)
        {
          $result[$key]["animals"] = [];
        }
        else
        {
          $result[$key]["animals"] = $exploded;
        }

        unset($result[$key]["total_count"]);
      }
    }

    public function fetchResults($queryString)
    {
      $params = $this->queryParams = getSearchParameters($queryString, $this->onFailure);
      
      $selectionQuery = $this->buildFullSelectionQuery($params);
      $selectionString = $selectionQuery["string"];
      $sortOrder = $params["order"];

      $dbh = getConnection();

      $statement = $dbh->prepare("
        SELECT name, country, array_to_string(animal, ',') AS animals, hardness,
          count(*) over() AS total_count
        FROM cheeses
        $selectionString
        ORDER BY 
          CASE :sortBy
            WHEN 'name' THEN name
            WHEN 'country' THEN country
            WHEN 'hardness' THEN hardness
            WHEN 'animal' THEN animal::text
          END
          $sortOrder, name
        LIMIT :limit
        OFFSET :offset
        "
      );

      $statement->bindParam(":limit", $params["limit"], PDO::PARAM_INT);
      $statement->bindParam(":offset", $params["offset"], PDO::PARAM_INT);
      $statement->bindParam(":sortBy", $params["sortBy"], PDO::PARAM_STR);


      foreach($selectionQuery["paramsToBind"] as $key=>&$toBind)
      {
        $statement->bindParam($key, $toBind, PDO::PARAM_STR);
      }

      $statement->execute();

      $result = $statement->fetchAll(PDO::FETCH_ASSOC);
      $matchCount = ($result ? $result[0]["total_count"] : 0);

      $this->cleanUpSearchResult($result);


      return(
      [
        "cheeses" => $result,
        "matchCount" => $matchCount
      ]);
    }
  }
?>
