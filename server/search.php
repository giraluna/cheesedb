<?php
  require_once "searchfetcher.php";

  header("Content-Type: application/json; charset=UTF-8");


  function returnError($errorMessage)
  {
    header("HTTP/1.1 400 Bad Request");
    die(json_encode(array("message" => $errorMessage)));
  }

  function returnOk($searchData)
  {
    header("HTTP/1.1 200 OK");
    echo $searchData;
  }

  $searchFetcher = new SearchFetcher("returnError");

  $searchData = $searchFetcher->fetchResults($_SERVER["QUERY_STRING"]);

  returnOk(json_encode($searchData));
?>
