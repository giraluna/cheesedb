<?php
  $reqeustErrorMessage = "Query must be in form name=Brie or country=ANY(France, Italy) [ANY|ALL|NOT]";

  function returnWrongParameterError($errorHandler, $errorMessage, $validParameters)
  {
    $errorHandler($errorMessage .
      " Valid parameters: [" . implode(", ", $validParameters) . "]"
    );
  }

  function getSearchParameters($queryString, $onError)
  {
    $searchParams =
    [
      "name" => NULL,
      "country" => NULL,
      "hardness" => NULL,
      "animal" => NULL
    ];

    $numericParams =
    [
      "offset" => 0,
      "limit" => 20,
    ];

    $numericParamLimits =
    [
      "limit" =>
      [
        "min" => 1,
        "max" => 100
      ]
    ];

    $stringParams =
    [
      "sortBy" => "name",
      "order" => "asc"
    ];

    $validStringParams =
    [
      "sortBy" => array("name", "country", "animal", "hardness"),
      "order" => array("asc", "desc")
    ];

    $params = array_merge($searchParams, $numericParams, $stringParams);

    $queryParams;
    parse_str($queryString, $queryParams);

    foreach($queryParams as $key=>$param)
    {
      if (array_key_exists($key, $searchParams))
      {
        $groups = getSelectionGroups($param, $onError);
        if ($groups)
        {
          $params[$key] = $groups;
        }
        else
        {
          $params[$key] =
          [
            "ANY" => [$param]
          ];
        }
      }
      else if (array_key_exists($key, $numericParams))
      {
        if (ctype_digit($param))
        {
          if ($numericParamLimits[$key])
          {
            $min = $numericParamLimits[$key][min];
            $max = $numericParamLimits[$key][max];

            if ($param < $min || $param > $max)
            {
              $onError("Numeric parameter $key needs to be in range $min - $max. Got $param");
            }
          }
          $params[$key] = $param;
        }
        else
        {
          $onError("Expected parameter $key to be numeric. Got $param.");
        }
      }
      else if (array_key_exists($key, $stringParams))
      {
        if (ctype_alpha($param))
        {
          $lower = strtolower($param);
          if (in_array($lower, $validStringParams[$key]))
          {
            $params[$key] = $lower;
          }
          else
          {
            returnWrongParameterError($onError, "Invalid parameter $param for $key.",
              array_values($validStringParams[$key]));
          }
        }
        else
        {
          returnWrongParameterError($onError, "Invalid parameter $param for $key.",
              array_values($validStringParams[$key]));
        }
      }
      else
      {
        returnWrongParameterError($onError, "Invalid parameter $key in search query.", array_keys($params));
      }
    }

    $params["ascii_name"] = $params["name"];

    return $params;
  }

  function getSelectionGroups($str, $onError)
  {
    $selectors = ["ALL", "ANY", "NOT"];
    $groups = [];
    
    foreach($selectors as $selector)
    {
      $initialMatcher = "/" . $selector . "\(/";
      $initialMatchIndex;
      if (preg_match($initialMatcher, $str, $initialMatches, PREG_OFFSET_CAPTURE))
      {
        $initialMatchIndex = $initialMatches[0][1] + strlen($selector) + 1;
      }
      else
      {
        continue;
      }


      $endMatchIndex;
      // Closing brace
      if (preg_match("/\)/", $str, $endMatches, PREG_OFFSET_CAPTURE, $initialMatchIndex))
      {
        $endMatchIndex = $endMatches[0][1];
      }
      else
      {
        $onError("No closing brace for selection group $selector in $str. " . $reqeustErrorMessage);
      }


      $matchLength = $endMatchIndex - $initialMatchIndex;
      $captured = substr($str, $initialMatchIndex, $matchLength);

      // Has another opening brace in captured string
      if (preg_match("/\(/", $captured))
      {
        $onError("No closing brace for selection group $selector in $str. " . $reqeustErrorMessage);
      }

      $groups[$selector] = preg_split("/\s*,\s*/", $captured);
    }

    if (count($groups) > 0)
    {
      return $groups;
    }
    else
    {
      return NULL;
    }
  }
?>
