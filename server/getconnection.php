<?php
  function getConnection()
  {
    $decoded = json_decode(file_get_contents("accessorcredentials.json"), true);
    
    $host = "localhost";
    $db = "cheese";

    $user = $decoded["userName"];
    $pass = $decoded["password"];
    
    $dsn = "pgsql:host=$host;dbname=$db;user=$user;password=$pass";

    $conn = new PDO($dsn);
    // require_once "pdotester.php";
    // $conn = new PDOTester($dsn);

    if(!$conn)
    {
      throw new Exception("Couldn't connect to database");
    }

    return $conn;
  }
?>
